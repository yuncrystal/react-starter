const path = require('path');
const webpack = require('webpack');
const dotenv = require('dotenv');
const fs = require('fs'); // to check if the file exists
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = (env) => {
    // Get the root path (assuming your webpack config is in the root of your project!)
    const currentPath = path.join(__dirname);


    // Create the fallback path (the production .env)
    const basePath = currentPath + '/.env';

    // We're concatenating the environment name to our filename to specify the correct env file!
    const envPath = basePath + '.' + env.ENVIRONMENT;

    // Check if the file exists, otherwise fall back to the production .env
    const finalPath = fs.existsSync(envPath) ? envPath : basePath;

    // Set the path parameter in the dotenv config
    const fileEnv = dotenv.config({ path: finalPath }).parsed;

    // reduce it to a nice object, the same as before (but with the variables from the file)
    const envKeys = Object.keys(fileEnv).reduce((prev, next) => {
        prev[`process.env.${next}`] = JSON.stringify(fileEnv[next]);
        return prev;
    }, {});

    const target = process.env.npm_lifecycle_event;

    return {
        entry: ["@babel/polyfill", './src/index.js'],
        output: {
            filename: 'acy-form.js',
            path: path.resolve(__dirname, 'dist'),
            library: 'Acy'
        },
        externals: {
            react: 'React',
            'react-dom': 'ReactDOM',
        },
        module: {
            rules: [
                {
                    test: /\.(js|jsx)$/,
                    exclude: /node_modules/,
                    use: ['babel-loader']
                },
                {
                    test: /\.(css|scss)$/,
                    use: [
                        // style-loader
                        { loader: 'style-loader' },
                        // css-loader
                        {
                            loader: 'css-loader',
                            options: {
                                modules: true
                            }
                        },
                        // sass-loader
                        { loader: 'sass-loader' }
                    ]
                },
                {
                    test: /\.(png|jpe?g|gif|svg)$/i,
                    loader: 'file-loader',

                    options: {
                        publicPath: (target === 'build-pro') || (target === 'build-dev') ? '/img' : '',
                        // outputPath: '../assets',
                        // publicPath: '/img',
                    },
                },
            ]
        },
        resolve: {
            modules: [
                "node_modules",
                'src'
            ],
            extensions: ['*', '.js', '.jsx', '.scss', '.css'],
            mainFiles: ['index']
        },

        plugins: [
            new CleanWebpackPlugin(),
            new webpack.HotModuleReplacementPlugin(),
            new webpack.DefinePlugin(envKeys),
            new HtmlWebpackPlugin({
                title: 'form',
                template: './src/index.ejs',
            }),
        ],
        devServer: {
            contentBase: './dist',
            historyApiFallback: true,
        }
    }
};