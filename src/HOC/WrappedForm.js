import React, { useEffect, useState, useRef } from 'react';
import { API_URL } from '../api/apiUrl';
import FormAPI from '../api/form_api';
import { useTranslation } from 'react-i18next';

export default (WrappedComponent) => {
    const FormPhoneInput = (props) => {
        const { t, i18n } = useTranslation();
        const [refer, setRefer] = useState('')
        const [lang, setLang] = useState('English')
        const [requestHost, setRequestHost] = useState('')
        const [firstName, setFirstName] = useState({ value: '', validate: false })
        const [lastName, setLastName] = useState({ value: '', validate: false })
        const [email, setEmail] = useState({ value: '', validate: false })
        const [countryCode, setCountryCode] = useState('')
        const [phone, setPhone] = useState({ value: '', validate: false })
        const [code, setCode] = useState({ value: '', validate: false })
        const [submitStatus, setSubmitStatus] = useState(true);
        const [modalOpen, setModalOpen] = useState(false);
        const [modalContent, setModalContent] = useState('');
        const [modalTitle, setModalTitle] = useState('');
        const [comments, setComments] = useState('');
        const selectRef = useRef();

        useEffect(() => {
            setRefer(document.referrer);
            if (document.documentElement.lang === 'zh') {
                setLang('Chinese')
            }
            setRequestHost(window.location.hostname)

        }, [])


        const handleModalOpen = () => {
            setModalOpen(true);
        };

        const handModalClose = () => {
            setModalOpen(false);
        }

        const handleFormRequest = async (url, data) => {
            try {
                const response = await FormAPI.post(url, data)
                setModalTitle(t('formFiled:modalTitle'))
                setModalContent(t('formFiled:modalContent'))
                handleModalOpen(true);
                
            } catch (err) {
                setModalTitle(t('formFiled:modalTitleFail'))
                setModalContent(t('formFiled:modalContentFail'))
                handleModalOpen(true);
            }
        }

        const handContactUs = (event) => {
            event.preventDefault();
            handleFormRequest(API_URL.CONTACTUS.CONTACT, {
                first_name: firstName.value,
                last_name: lastName.value,
                email: email.value,
                country_code: countryCode,
                phone: phone.value,
                code: code.value,
                comments: comments,
                refer: refer,
                affilate_url: 'none',
                email_lang: lang,
                request_host: requestHost
            })
        }

        const webinerRegister = (event) => {
            event.preventDefault();
            handleFormRequest(API_URL.WEBINAR.REGISTER, {
                course: selectRef.current.value,
                first_name: firstName.value,
                last_name: lastName.value,
                email: email.value,
                country_code: countryCode,
                phone: phone.value,
                code: code.value,
                refer: refer,
                affilate_url: 'none',
                email_lang: lang,
                request_host: requestHost
            })
        }

        return <WrappedComponent
            refer={refer}
            lang={lang}
            requestHost={requestHost}
            firstName={firstName}
            lastName={lastName}
            email={email}
            countryCode={countryCode}
            phone={phone}
            code={code}
            comments={comments}
            submitStatus={submitStatus}
            modalOpen={modalOpen}
            setFirstName={setFirstName}
            setLastName={setLastName}
            setEmail={setEmail}
            setCountryCode={setCountryCode}
            setPhone={setPhone}
            setCode={setCode}
            setComments={setComments}
            setSubmitStatus={setSubmitStatus}
            setModalOpen={setModalOpen}
            modalTitle={modalTitle}
            modalContent={modalContent}
            handModalClose={handModalClose}
            webinerRegister={webinerRegister}
            handContactUs={handContactUs}
            selectRef={selectRef}
            {...props}
        />;
    }

    return FormPhoneInput;

}