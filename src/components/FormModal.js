
import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import { Box } from '@material-ui/core';
import Modal from '@material-ui/core/Modal';
import { withTranslation } from 'react-i18next';
import Typography from '@material-ui/core/Typography';
import BG from './popup_background.svg';

const useStyles = makeStyles((theme) => {
    return {
        paper: {
            backgroundImage: `url(${BG})`,
            backgroundPosition: '100% 100%',
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            margin: 'auto',
            width: 500,
            height: 328,
            borderRadius: 8,
            backgroundColor: '#FFFFFF',
            border: '1px solid #979797',
            boxShadow: '2px 2px 10px -5px #000',
            padding: '40px 70px',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'space-between',
            '&:focus': {
                outlineWidth: 0
            },
            [theme.breakpoints.down('xs')]: {
                width: 'calc(100% - 32px)',
                padding:'40px 10px'
            }
        },
        title: {
            fontSize: 22,
            color: '#01254f',
            fontWeight: 400,
            paddingTop: 37,
        },
        content: {
            fontSize: 16,
            lineHeight: '30px',
            color: '#4a4a4a',
            textAlign: 'left',
        },
        closeBtn: {
            height: 42,
            width: 230,
            fontSize: 16,
            borderRadius: 4,
            color: '#FFFFFF',
            backgroundColor: '#013b81',
            cursor: 'pointer',
            fontWeight: 600,
            padding:0,
            border:0,
            '&:focus': {
                outlineWidth: 0
            }
        }
    }
})

const FormModal = ({ open, handleClose, title, content, btnContent }) => {
    const classes = useStyles();
    return (
        <Modal
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            open={open}
            onClose={handleClose}
        >
            <Typography className={classes.paper} component="div">
                <div className={classes.title}>{title}</div>
                <div className={classes.content}>{content}</div>
                <button className={classes.closeBtn} onClick={handleClose}>{btnContent}</button>
            </Typography>
        </Modal>
    )

}


FormModal.propTypes = {
    open: PropTypes.bool.isRequired,
    handleClose: PropTypes.func.isRequired,
    title: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,
    btnContent: PropTypes.string.isRequired,
};
export default withTranslation()(FormModal);