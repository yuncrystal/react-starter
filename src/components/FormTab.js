import React from 'react';
import Box from '@material-ui/core/Box';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';

const useStyles = makeStyles(() => {
    return {
        root: {
            marginBottom: 40
        },
        tabItem: {
            width: '50%'
        },
        title: {
            fontSize: 16,
            marginBottom: 12,
            textAlign: 'center',
            letterSpacing:1.07,
            lineHeight:'22px'

        },
        titleLeft: {
            color: props => props.confirmStage ? '#17A5DC' : '#01254F'
        },
        titleRight: {
            color:props => props.confirmStage ? '#01254F'  : '#D5D5D5'
        },
        lineLeft: {
            height: 4,
            width: '100%',
            background: props => props.confirmStage ? '#17A5DC' : '#01254F'
        },
        lineRight: {
            height: 4,
            width: '100%',
            background: props => props.confirmStage ? '#01254F'  : '#D8D8D8'
        }

    }
})

const FormTab = ({ tabOneContent, tabTwoContent, confirmStage }) => {
    const classes = useStyles({confirmStage});
    return (
        <Box display='flex' className={classes.root}>
            <Box className={classes.tabItem}>
                <div className={`${classes.title} ${classes.titleLeft}`}>{tabOneContent}</div>
                <div className={classes.lineLeft} />
            </Box>
            <Box className={classes.tabItem}>
                <div className={`${classes.title} ${classes.titleRight}`}>{tabTwoContent}</div>
                <div className={classes.lineRight} />
            </Box>

        </Box>
    )
}

FormTab.propTypes = {
    tabOneContent: PropTypes.string.isRequired,
    tabTwoContent: PropTypes.string.isRequired,
    confirmStage: PropTypes.bool.isRequired,
};
export default FormTab;