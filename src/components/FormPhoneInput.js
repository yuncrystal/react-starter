
import React, { Fragment, useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import { Box } from '@material-ui/core';
import PhoneVerificationModal from './PhoneVerificationModal';
import libphonenumber from '../libphonenumber.full.json.js';
import { withTranslation } from 'react-i18next';
import { parsePhoneNumberFromString } from 'libphonenumber-js'
import i18nIsoCountries from 'i18n-iso-countries';
import { API_URL } from '../api/apiUrl';
import FormAPI from '../api/form_api';
import ArrowDown from './arrowDown.png';
import ArrowUp from './arrowUP.png';
import Check from './checked.png';

const useStyles = makeStyles((theme) => {
    return {
        root: {
            width: '100%',
            marginBottom: 20,
            [theme.breakpoints.down('sm')]: {
                marginBottom: 16,
            }
        },

        label: {
            marginBottom: 8,
            letterSpacing: 0.8,
            lineHeight: '22px',
            fontSize: 16,
            [theme.breakpoints.down('sm')]: {
                fontSize: 12,
                marginBottom: 6,
            }
        },
        labelDisable: {
            color: '#C7C7C7'
        },
        countryCode: {
            height: 40,
            boxSizing: 'border-box',
            border: '1px solid #C6C6C6',
            borderRadius: 4,
            backgroundColor: '#FFFFFF',
            padding: '0 12px',
            fontSize: 16,
            color: '#333333',
            letterSpacing: 0.8,
            lineHeight: '22px',
            width: '100%',
            display: 'flex',
            alignItems: 'center',
            cursor: 'pointer',
            [theme.breakpoints.down('sm')]: {
                height: 32,
                fontSize: 12,
                justifyContent: 'center'
            }
        },
        left: {
            width: 100,
            [theme.breakpoints.down('md')]: {
                width: 120,
            },
            [theme.breakpoints.down('sm')]: {
                width: 60,
            }
        },
        right: {
            width: 'calc(100% - 128px)',
            [theme.breakpoints.down('md')]: {
                width: 'calc(100% - 160px)',
            },
            [theme.breakpoints.down('sm')]: {
                width: 'calc(100% - 74px)',
            }
        },
        options: {
            border: '1px solid #C6C6C6',
            borderRadius: 4,
            position: 'absolute',
            flexDirection: 'column',
            height: 190,
            overflow: 'scroll',
            backgroundColor: '#FFFFFF',
            marginTop: 8,
            zIndex: 1,
            [theme.breakpoints.down('sm')]: {
                height: 155,
                fontSize: 12,
            }
        },
        optionItme: {
            height: 37,
            paddingLeft: 12,
            cursor: 'pointer',
            fontSize: 16,
            minHeight: 37,
            [theme.breakpoints.down('sm')]: {
                fontSize: 12,
                height: 31,
                minHeight: 31,
            }
        },
        grey: {
            backgroundColor: '#F2F2F2'
        },
        input: {
            height: 40,
            boxSizing: 'border-box',
            border: '1px solid #C6C6C6',
            borderRadius: 4,
            backgroundColor: '#FFFFFF',
            padding: '0 12px',
            fontSize: 16,
            color: '#333333',
            letterSpacing: 0.8,
            lineHeight: '22px',
            width: '100%',
            '&:hover': {
                fontStyle: 'italic'
            },
            '&:focus': {
                border: '1px solid #333333',
                outlineWidth: 0,
                fontStyle: 'normal'
            },
            [theme.breakpoints.down('sm')]: {
                height: 32,
                fontSize: 12,
            }
        },
        inputError: {
            backgroundColor: 'rgba(141,42,54,0.03)',
            border: '1px solid #8D2A36',
        },
        default: {
            display: 'none'
        },
        error: {
            display: 'block',
            fontSize: 16,
            color: '#B33C4B',
            letterSpacing: 0.8,
            lineHeight: '22px',
            marginTop: 8,
            textAlign: 'left',
            [theme.breakpoints.down('sm')]: {
                fontSize: 12,
                marginTop: 6,
            }
        },
        arrow: {
            position: 'absolute',
            width: 12,
            right: 16,
            cursor: 'pointer',
            [theme.breakpoints.down('md')]: {
                position: 'static',
                marginLeft: 8
            },
            [theme.breakpoints.down('sm')]: {
                width: 6,
            }
        },
        check: {
            position: 'absolute',
            width: 14,
            top: 0,
            bottom: 0,
            right: 16,
            margin: 'auto',
            cursor: 'pointer',
            display: props => props.phoneChecked ? 'block' : 'none'
        },
        codeCheck: {
            position: 'absolute',
            width: 14,
            top: 0,
            bottom: 0,
            right: 16,
            margin: 'auto',
            cursor: 'pointer',
            display: props => props.codeChecked ? 'block' : 'none'
        },
        codeBtn: {
            height: 40,
            width: 100,
            border: '1px solid #C6C6C6',
            borderRadius: 4,
            letterSpacing: 0.8,
            fontSize: 16,
            lineHeight: '22px',
            color: '#333333',
            cursor: 'pointer',
            backgroundColor: '#fff',
            padding: 0,

            '&:focus': {
                outlineWidth: 0,
                backgroundColor: '#fff',
            },
            '&:hover': {
                backgroundColor: '#fff',
            },
            '&:disabled': {
                color: '#FFFFFF',
                backgroundColor: '#01254F',
                border: 'none',
                cursor: 'not-allowed',
            },
            [theme.breakpoints.down('md')]: {
                width: 120
            },
            [theme.breakpoints.down('sm')]: {
                height: 32,
                fontSize: 12,
                width: 60
            }
        }
    }
})

const FormPhoneInput = (props) => {
    const { t, i18n, requestHost, handleCountryChange, handleChangePhone, phoneVal, handleChangeCode, codeVal } = props;
    const { countries } = libphonenumber;

    const classes = useStyles({ phoneChecked: phoneVal.validate, codeChecked: codeVal.validate });
    const [phoneError, setPhoneError] = useState(null);
    const [codeError, setCodeError] = useState(null);
    const [countryCode, setCountryCode] = useState({ abbr: 'CN', code: '86' });
    const [optionsStatus, setOptionsStatus] = useState(false);
    const [sentCodeSuccess, setSentCodeSuccess] = useState(false);
    const [open, setOpen] = useState(false);
    const [codeHidden, setCodeHidden] = useState(false);
    const [isMobile, setIsMobile] = useState(false);
    const wrapperRef = useRef(null);
    let countryCount = 0;

    function handleClickOutside(event) {
        if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
            setOptionsStatus(false);
        }
    }

    const resizeWindow = () => {
        if (window.innerWidth < 768) {
            setIsMobile(true)
        } else {
            setIsMobile(false)
        }

        

    }

    useEffect(() => {
        resizeWindow();
        window.addEventListener('resize', resizeWindow);

        return () => {
            document.removeEventListener("resize", resizeWindow);
        }
    }, [])

    useEffect(() => {
        document.addEventListener("mousedown", handleClickOutside);
        handleCountryChange('86')
        if (requestHost === 'www.acyindo.com' || requestHost === 'www.staging.acyindo.com' || requestHost === 'cn.acyindo.com' || requestHost === 'cn.staging.acyindo.com') {
            setCountryCode({
                ...countryCode,
                code: '62'
            });
            handleCountryChange(value[0])
        } else if (requestHost === 'www.acysecurities.com' || requestHost === 'www.staging.acysecurities.com' || requestHost === 'cn.acysecurities.com' || requestHost === 'cn.staging.acysecurities.com') {
            setCountryCode({
                ...countryCode,
                code: '61'
            });
            handleCountryChange(value[0])
        }
        return () => {
            document.removeEventListener("mousedown", handleClickOutside);
        };
    }, [])

    const handleOpen = () => {
        setOpen(true);
    };

    const handModalClose = () => {
        setOpen(false);
    }

    const phoneValidate = (validate) => {
        handleChangePhone({
            ...phoneVal,
            validate: validate
        })
    }

    const codeValidate = (validate) => {
        handleChangeCode({
            ...codeVal,
            validate: validate
        })
    }

    const getValidationCode = async (e) => {
        e.preventDefault();
        if (!phoneVal.validate) {
            if (phoneVal.value === '' || phoneVal.value === null) {
                setPhoneError(t('formFiled:fieldRequired'));
            } else {
                setPhoneError(t('formFiled:error_phone_exist'));
            }
            return false;
        }
        try {
            const response = await FormAPI.post(API_URL.PHONE.VALIDATION, {
                phone: phoneVal.value,
                country: countryCode.code
            })
            if (response.data.status_code >= 400) {
                setPhoneError(t('formFiled:error_phone_exist'));
                phoneValidate(false)
                setSentCodeSuccess(false)
            } else {
                handleOpen();
                setSentCodeSuccess(true)
            }

            console.log('response:', response)
        } catch (err) {
            console.log('err:', err)
        }
    }


    return (
        <Fragment>
            <Box className={classes.root} display='flex' flexDirection='column'>
                <label className={classes.label}>{t('formFiled:phone')}</label>
                <Box display='flex' justifyContent='space-between'>
                    <Box ref={wrapperRef} className={classes.left}>
                        <Box display='flex' position='relative' onClick={() => setOptionsStatus(!optionsStatus)}>
                            <Box className={classes.countryCode} >
                                +{countryCode.code}
                                <img className={classes.arrow} src={optionsStatus ? ArrowUp : ArrowDown} alt="arrow.svg" />
                            </Box>

                        </Box>

                        <Box className={classes.options} display={optionsStatus ? 'flex' : 'none'} >
                            {
                                _.map(countries, (value, key) => {
                                    countryCount++
                                    return <Box display='flex' alignItems='center' className={`${classes.optionItme} ${countryCount % 2 === 0 ? classes.grey : ''}`} key={key}
                                        onClick={() => {
                                            if (key === 'VN' || key === 'UK' || key === 'HU') {
                                                setCodeHidden(true);
                                                setCodeError(null);
                                                codeValidate(true)
                                            } else {
                                                setCodeHidden(false)
                                                codeValidate(false)
                                            }
                                            setCountryCode({
                                                ...countryCode,
                                                code: value[0]
                                            })
                                            handleCountryChange(value[0])
                                            setOptionsStatus(false)
                                        }}>{i18nIsoCountries.getName(key, i18n.language)} +{value[0]}
                                    </Box>
                                })
                            }
                        </Box>
                    </Box>
                    <Box className={classes.right}>
                        <Box display='flex' position='relative'>
                            <input className={`${classes.input} ${phoneError ? classes.inputError : ''}`} type="text" name='phone'
                                onChange={(e) => {
                                    handleChangePhone({
                                        ...phoneVal,
                                        value: e.target.value
                                    })
                                }}
                                onBlur={(e) => {
                                    let value = e.target.value.trim();
                                    if (value == "") {
                                        setPhoneError(t('formFiled:fieldRequired'));
                                        phoneValidate(false)
                                    } else {
                                        const phoneNumber = parsePhoneNumberFromString(value, countryCode.abbr);
                                        if (phoneNumber) {
                                            setPhoneError(null);
                                            phoneValidate(true)
                                        } else {
                                            setPhoneError(t('formFiled:error_phone'));
                                            phoneValidate(false)
                                        }
                                    }
                                }} />
                            <img className={classes.check} src={Check} alt="check.svg" />
                        </Box>

                        <span className={phoneError ? classes.error : ''}>{phoneError}</span>
                    </Box>

                </Box>
            </Box>
            <Box className={classes.root} display={codeHidden ? 'none' : 'flex'} flexDirection='column'>
                <label className={classes.label}>{t('formFiled:authCode')}</label>
                <Box display='flex' justifyContent='space-between'>
                    <button className={classes.codeBtn}
                        disabled={sentCodeSuccess}
                        onClick={getValidationCode}>
                        {sentCodeSuccess ? t('formFiled:sent') : isMobile ? t('formFiled:getCodeAbbr') : t('formFiled:getCode')}
                    </button>
                    <Box className={classes.right}>
                        <Box display='flex' position='relative'>
                            <input className={classes.input} type="text" name='code'
                                onChange={(e) => {
                                    handleChangeCode({
                                        ...codeVal,
                                        value: e.target.value
                                    })
                                }}
                                onBlur={(e) => {
                                    let value = e.target.value.trim();
                                    if (value == "") {
                                        setCodeError(t('formFiled:fieldRequired'));
                                        codeValidate(false)
                                    } else {
                                        setCodeError(null);
                                        codeValidate(true)
                                    }
                                }} />
                            <img className={classes.codeCheck} src={Check} alt="check.svg" />
                        </Box>

                        <span className={codeError ? classes.error : ''}>{codeError}</span>
                    </Box>
                </Box>
            </Box>
            <PhoneVerificationModal open={open} handleClose={handModalClose} />
        </Fragment>


    )

}


FormPhoneInput.propTypes = {
    requestHost: PropTypes.string.isRequired,
    handleCountryChange: PropTypes.func.isRequired,
    handleChangePhone: PropTypes.func.isRequired,
    handleChangeCode: PropTypes.func.isRequired,
};
export default withTranslation()(FormPhoneInput);