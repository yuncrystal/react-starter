
import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import { Box } from '@material-ui/core';

const useStyles = makeStyles((theme) => {
    return {
        submit: {
            width: '100%',
            height: 48,
            borderRadius: 4,
            backgroundColor: '#43AA15 !important',
            border: 'none',
            fontWeight: '600',
            letterSpacing: 0.8,
            fontSize: 16,
            lineHeight: '22px',
            color: '#FFFFFF',
            cursor: 'pointer',
            '&:hover': {
                backgroundColor: '#43AA15 !important',
            },
            '&:disabled': {
                backgroundColor: '#E9E9E9 !important',
                color: '#D1D1D1',
                cursor: 'not-allowed'
            },
            '&:focus': {
                outlineWidth: 0,
            },
            [theme.breakpoints.down('sm')]: {
                height: 32,
                fontSize: 12,
            }
        },
    }
})

const SubmitBtn = ({ content, disabled }) => {
    const classes = useStyles();
    return (
        <input disabled={disabled} className={classes.submit} type="submit" value={content} />
    )
}

SubmitBtn.propTypes = {
    content: PropTypes.string.isRequired,
    disabled: PropTypes.bool.isRequired,
};
export default SubmitBtn;