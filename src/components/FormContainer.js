import React from 'react';
import Box from '@material-ui/core/Box';
import CssBaseline from '@material-ui/core/CssBaseline'
import { ThemeProvider, createMuiTheme, makeStyles } from '@material-ui/core/styles';




const theme = createMuiTheme({
    typography: {
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            '"PingFang SC"',
            '"Hiragino Sans GB"',
            '"Microsoft YaHei"',
            '"Helvetica Neue"',
            'Helvetica',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"'
        ]
    },
    breakpoints: {
        values: {
            xs: 0,
            sm: 600,
            md: 768,
            lg: 1200,
            xl: 1920
        },
    },
    palette: {
        text: {
            title: "#01254F",
            primary: "#333333",
        }
    }
});
const useStyles = makeStyles((theme) => {
    console.log('container theme:',theme)
    return {
        root: {
            padding: 0,
            fontFamily: '-apple-system, BlinkMacSystemFont, "Segoe UI", "PingFang SC", "Hiragino Sans GB", "Microsoft YaHei", "Helvetica Neue", Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"',
            color: '#333333'
        },
        formWrap: {
            margin: 'auto',
        }
    }
})

const FormContainer = (props) => {
    const classes = useStyles();
    return (
        <React.Fragment>
            {/* <CssBaseline /> */}
            <ThemeProvider theme={theme}>
                {props.children}
            </ThemeProvider>
        </React.Fragment>
    )
}

export default FormContainer;