
import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import { withTranslation } from 'react-i18next';

const useStyles = makeStyles((theme) => {
    return {
        paper: {
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            margin: 'auto',
            width: 394,
            height: 274,
            borderRadius: 4,
            backgroundColor: '#FFFFFF',
            border: '1px solid #979797',
            boxShadow: '0 5px 10px 1px rgba(0,0,0,0.5)',
            padding: '76px 60px',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'space-between',
            boxSizing:'border-box',
            '&:focus': {
                outlineWidth: 0
            },
            [theme.breakpoints.down('xs')]: {
                width: 'calc(100% - 32px)',
                padding: '60px',
            }
        },
        content: {
            fontSize: 20,
            fontWeight: 600,
            letterSpacing: 1,
            lineHeight: '27px',
            marginBottom: 28,

        },
        closeBtn: {
            height: 40,
            width: 100,
            fontSize: 16,
            borderRadius: 4,
            color: '#FFFFFF',
            backgroundColor: '#01254F',
            cursor: 'pointer',
            padding:0,
            '&:focus': {
                outlineWidth: 0
            }
        }
    }
})

const PhoneVerificationModal = ({ t, open, handleClose }) => {
    const classes = useStyles();
    return (
        <Modal
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            open={open}
            onClose={handleClose}
        >
            <div className={classes.paper}>
                <div className={classes.content}>{t('formFiled:sent_code_success')}</div>
                <button className={classes.closeBtn} onClick={handleClose}>{t('common:close')}</button>

            </div>
        </Modal>
    )

}


PhoneVerificationModal.propTypes = {
    open: PropTypes.bool.isRequired,
    handleClose: PropTypes.func.isRequired,
};
export default withTranslation()(PhoneVerificationModal);