
import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { Box } from '@material-ui/core';

const useStyles = makeStyles((theme) => {
    const { palette } = theme;
    return {
        root: {
            textAlign: 'center',
            marginBottom: 40
        },
        title: {
            color: palette.text.title,
            fontSize: 22,
            paddingBottom: 20
        },
        subTitle: {
            color: palette.text.primary,
            width:467,
            textAlign:'center'
        }
    }
})


const FormHeader = ({ title, subTitle }) => {
    const classes = useStyles();
    return (
        <Box className={classes.root} display="flex" flexDirection="column" alignItems="center">
            <Typography className={classes.title} component="div">
                {title}
            </Typography>

            <Typography className={classes.subTitle} component="div">
                {subTitle}
            </Typography>


        </Box>

    )

}


FormHeader.propTypes = {
    title: PropTypes.string.isRequired,
    subTitle: PropTypes.string
};
export default FormHeader;