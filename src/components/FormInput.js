
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import { Box } from '@material-ui/core';
import { withTranslation } from 'react-i18next';
import validator from 'validator';
import Check from './checked.png';
import XRegExp from "xregexp";

const useStyles = makeStyles((theme) => {
    return {
        root: {
            width: '100%',
            marginBottom: 20,
            [theme.breakpoints.down('sm')]: {
                marginBottom: 16,
            }
        },
        label: {
            marginBottom: 8,
            letterSpacing: 0.8,
            lineHeight: '22px',
            fontSize: 16,
            [theme.breakpoints.down('sm')]: {
                fontSize: 12,
                marginBottom: 6
            }
        },
        input: {
            height: 40,
            width: '100%',
            boxSizing: 'border-box',
            border: '1px solid #C6C6C6',
            borderRadius: 4,
            backgroundColor: '#FFFFFF',
            padding: '0 12px',
            fontSize: 16,
            color: '#333333',
            letterSpacing: 0.8,
            lineHeight: '22px',
            '&:hover': {
                fontStyle: 'italic'
            },
            '&:focus': {
                border: '1px solid #333333',
                outlineWidth: 0,
                fontStyle: 'normal'
            },
            [theme.breakpoints.down('sm')]: {
                height: 32
            }
        },

        inputError: {
            backgroundColor: 'rgba(141,42,54,0.03)',
            border: '1px solid #8D2A36',
        },
        error: {
            display: 'block',
            fontSize: 16,
            color: '#B33C4B',
            letterSpacing: 0.8,
            lineHeight: '22px',
            marginTop: 8,
            textAlign:'left',
            [theme.breakpoints.down('sm')]: {
                fontSize: 12,
                marginTop: 6
            }
        },
        check: {
            position: 'absolute',
            width: 14,
            top: 0,
            bottom: 0,
            right: 16,
            margin: 'auto',
            cursor: 'pointer',
            display: props => props.checked ? 'block' : 'none'
        }
    }
})

const FormInput = ({ t, required, label, val, handleChange, type }) => {

    const [error, setError] = useState(null);
    const props = { checked: val.validate }
    const classes = useStyles(props);


    const handleValidate = (validate, error) => {
        setError(error);
        handleChange({
            ...val,
            validate: validate
        })
    }

    return (
        <Box className={classes.root} display='flex' flexDirection='column'>
            <label className={classes.label}>{label}</label>
            <Box display='flex' position='relative'>
                <input className={`${classes.input} ${error ? classes.inputError : ''}`}
                    type="text"
                    onChange={(e) => {
                        handleChange({
                            ...val,
                            value: e.target.value
                        })
                    }}
                    onBlur={(e) => {
                        if (required) {
                            let value = e.target.value.trim();
                            if (value == "") {
                                handleValidate(false, t('formFiled:fieldRequired'))
                            } else {
                                if(type==='email' && !validator.isEmail(value)){
                                    handleValidate(false, t('formFiled:error_email'))
                                }else if(type === 'firstName'){
                                    var regex = XRegExp('^\\pL{1,40}$');
                                    if(!regex.test(value)){
                                        handleValidate(false, t('formFiled:first_name_error'))
                                    }else {
                                        handleValidate(true, null)
                                    }
                                }else if(type === 'lastName'){
                                    var regex = XRegExp('^\\pL{1,80}$');
                                    if(!regex.test(value)){
                                        handleValidate(false, t('formFiled:last_name_error'))
                                    }else {
                                        handleValidate(true, null)
                                    }
                                }
                                else {
                                    handleValidate(true, null)
                                }
                            }
                        }
                    }} />
                <img className={classes.check} src={Check} alt="check.svg" />
            </Box>

            <span className={error ? classes.error : ''}>{error}</span>
        </Box>

    )

}


FormInput.propTypes = {
    label: PropTypes.string.isRequired,
    handleChange: PropTypes.func.isRequired,
    required: PropTypes.bool.isRequired,
    type: PropTypes.oneOf(['email','firstName','lastName']),
};
export default withTranslation()(FormInput);