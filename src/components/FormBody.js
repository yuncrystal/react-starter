
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles((theme) => {
    return {
        root: {
            display: 'flex',
            flexDirection: 'column',
            background:props => props.background
        },
    }
})

const FormBody = ({ handleSubmit, children,background }) => {
    const classes = useStyles({
        background:background
    });
    return (
        <Typography component="div">
            <form autoComplete="off" className={classes.root} onSubmit={handleSubmit}>
                {children}
            </form>
        </Typography>
    )

}


FormBody.propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    background: PropTypes.string
};

FormBody.defaultProps = {
    background: '#fff'
  };
export default FormBody;