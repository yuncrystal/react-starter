
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import { Box } from '@material-ui/core';
import { withTranslation } from 'react-i18next';

const useStyles = makeStyles((theme) => {
    return {
        root: {
            width: '100%',
            marginBottom: 20,
            [theme.breakpoints.down('sm')]: {
                marginBottom: 16,
            }
        },
        label: {
            marginBottom: 8,
            letterSpacing: 0.8,
            lineHeight: '22px',
            fontSize: 16,
            [theme.breakpoints.down('sm')]: {
                fontSize: 12,
                marginBottom: 6
            }
        },
        textarea: {
            height: 200,
            width: '100%',
            border: '1px solid #C6C6C6',
            borderRadius: 4,
            background: '#ffffff',
            fontSize:16,
            padding:'8px 12px',
            boxSizing:'border-box',
            outlineWidth:0
        },
        optional:{
            color:'#9C9C9C',
            fontSize:16
        }
    }
})

const TextArea = ({ t, label, handleChange,placeholder,required }) => {

    const classes = useStyles();

    return (
        <Box className={classes.root} display='flex' flexDirection='column'>
            <label className={classes.label}>{label} <span className={classes.optional}> {!required ? t('formFiled:optional') : ''} </span> </label>
            <textarea
                onChange={handleChange}
                className={classes.textarea}
                placeholder={placeholder}
            />

        </Box>

    )

}


TextArea.propTypes = {
    label: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    handleChange: PropTypes.func.isRequired,
    required: PropTypes.bool.isRequired,
};

TextArea.defaultProps = {
    required: false
  };

export default withTranslation()(TextArea);