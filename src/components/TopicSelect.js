
import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import { Box } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import ArrowDown from './arrowDown.png';
import ArrowUp from './arrowUP.png';

const useStyles = makeStyles((theme) => {
    return {
        root: {
            width: '100%',
            marginBottom: 20,
            [theme.breakpoints.down('sm')]: {
                marginBottom: 16,
            }
        },
        label: {
            marginBottom: 8,
            letterSpacing: 0.8,
            lineHeight: '22px',
            fontSize: 16,
            [theme.breakpoints.down('sm')]: {
                fontSize: 12,
                marginBottom: 6,
            }
        },
        select: {
            WebkitAppearance: 'none',
            appearance: 'none',
            MozAppearance: 'none',
            width: '100%',
            height: 40,
            boxSizing: 'border-box',
            border: '1px solid #C6C6C6',
            borderRadius: 4,
            backgroundColor: '#FFFFFF',
            padding: '0 12px',
            fontSize: 16,
            color: '#333333',
            letterSpacing: 0.8,
            lineHeight: '22px',
            cursor: 'pointer',
            backgroundImage: props => `url(${props.optionsStatus ? ArrowUp : ArrowDown}) !important`,
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'right 16px center',
            '&:focus': {
                outlineWidth: 0,
                fontStyle: 'normal'
            },
            [theme.breakpoints.down('sm')]: {
                height: 32,
                fontSize: 12,
                backgroundSize: 6
            }

        },
        option: {
            display: 'none'
        },
        options: {
            border: '1px solid #C6C6C6',
            borderRadius: 4,
            position: 'absolute',
            flexDirection: 'column',
            height: 190,
            overflow: 'scroll',
            backgroundColor: '#FFFFFF',
            marginTop: 8,
            width: '100%',
            zIndex: 1,
            top: 40,
            [theme.breakpoints.down('sm')]: {
                height: 155,
                fontSize: 12,
                top: 32,
            }
        },
        optionItme: {
            height: 37,
            minHeight: 37,
            paddingLeft: 12,
            cursor: 'pointer',
            fontSize: 16,
            [theme.breakpoints.down('sm')]: {
                height: 31,
                minHeight: 31,
                fontSize: 12,
            }
        },
        grey: {
            backgroundColor: '#F2F2F2'
        },
        arrow: {
            position: 'absolute',
            width: 12,
            top: 0,
            bottom: 0,
            right: 16,
            margin: 'auto',
            cursor: 'pointer'
        }

    }
})

const TopicSelect = ({ selectRef, name, label, options, selectorId }) => {

    const [optionsStatus, setOptionsStatus] = useState(false);
    const wrapperRef = useRef(null);
    const classes = useStyles({ optionsStatus: optionsStatus });
    function handleClickOutside(event) {
        if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
            setOptionsStatus(false);
        }
    }

    useEffect(() => {
        document.addEventListener("mousedown", handleClickOutside);
        return () => {
            // Unbind the event listener on clean up
            document.removeEventListener("mousedown", handleClickOutside);
        };
    }, [])

    return (
        <Typography component="div">
            <Box  className={classes.root} display='flex' flexDirection='column'>
                <label className={classes.label}>{label}</label>
                <Box ref={wrapperRef} display='flex' position='relative'>
                    <select className={classes.select} name={name}
                        id={selectorId}
                        ref={selectRef}
                        onClick={(e) => {
                            setOptionsStatus(!optionsStatus)
                        }}
                    >
                        {
                            options.map((option, index) => {
                                return <option className={classes.option} key={index} value={option}>{option}</option>
                            })
                        }
                    </select>
                    <Box className={classes.options} display={optionsStatus ? 'flex' : 'none'}>
                        {
                            options.map((course, index) => {
                                return <Box display='flex' alignItems='center' key={index}
                                    className={`${classes.optionItme} ${index % 2 === 1 ? classes.grey : ''}`}
                                    onClick={(e) => {
                                        setOptionsStatus(false);
                                        document.getElementById(selectorId).value = course;
                                    }}>
                                    {course}
                                </Box>
                            })
                        }
                    </Box>
                </Box>
            </Box>
        </Typography>


    )

}


TopicSelect.propTypes = {
    selectorId: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    options: PropTypes.array.isRequired,
};
export default TopicSelect;