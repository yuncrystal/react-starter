import React from 'react';
import { withTranslation } from 'react-i18next';


function Test({ t, i18n }) {
    console.log('t:',t);
    console.log('i18n:',i18n);
    return(
        <div>
            <p>{t('title')}</p>
        </div>
    )

}

export default withTranslation()(Test);