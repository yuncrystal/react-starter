import React, { useState, useEffect } from 'react';
import FormContainer from '../../components/FormContainer'
import FormBody from '../../components/FormBody';
import FormInput from '../../components/FormInput';
import FormPhoneInput from '../../components/FormPhoneInput';
import FormModal from '../../components/FormModal';
import SubmitBtn from '../../components/SubmitBtn';
import NextBtn from '../../components/NextBtn';
import { withTranslation } from 'react-i18next';
import WrappedForm from '../../HOC/WrappedForm';
import FormTab from '../../components/FormTab';
import TextArea from '../../components/TextArea';
import Box from '@material-ui/core/Box';


function ContactUs(props) {
    const { t, requestHost } = props;
    const { firstName, lastName, email, phone, code, comments, submitStatus, modalOpen, modalContent, modalTitle } = props;
    const { setFirstName, setLastName, setEmail, setCountryCode, setPhone, setCode, setComments, setSubmitStatus } = props;
    const { handModalClose, handContactUs } = props;
    const [confirmStage, setConfirmStage] = useState(false);
    const [firstStage, setFirstStage] = useState(false);

    useEffect(() => {
        if (firstName.validate && lastName.validate && email.validate && phone.validate && code.validate) {
            setSubmitStatus(false)
        } else {
            setSubmitStatus(true)
        }
    }, [firstName, lastName, email, phone, code])

    useEffect(() => {
        if (firstName.validate && lastName.validate && email.validate) {
            setFirstStage(true)
        } else {
            setFirstStage(false)
        }
    }, [firstName, lastName, email])

    return (
        <FormContainer>
            <FormBody handleSubmit={handContactUs}>
                <FormTab
                    tabOneContent={t('formFiled:personalDetail')}
                    tabTwoContent={t('formFiled:Confirmation')}
                    confirmStage={confirmStage} />
                <Box display='flex'>
                    <Box width='100%' display={!confirmStage ? 'block' : 'none'}>
                        <FormInput
                            required={true}
                            label={t('formFiled:firstName')}
                            handleChange={(value) => setFirstName(value)}
                            val={firstName}
                            type={'firstName'}
                        />
                        <FormInput
                            required={true}
                            label={t('formFiled:lastName')}
                            handleChange={(value) => setLastName(value)}
                            val={lastName}
                            type={'lastName'}
                        />
                        <FormInput
                            required={true}
                            label={t('formFiled:email')}
                            type='email'
                            handleChange={(value) => setEmail(value)}
                            val={email}
                        />
                        <NextBtn
                            disabled={!firstStage}
                            clickNext={() => {
                                setConfirmStage(true)
                                setFirstStage(true)
                            }}
                            content={t('common:next')} />
                    </Box>
                    <Box width='100%' display={confirmStage ? 'block' : 'none'}>
                        <FormPhoneInput
                            requestHost={requestHost}
                            handleCountryChange={(value) => setCountryCode(value)}

                            handleChangePhone={(value) => setPhone(value)}
                            phoneVal={phone}

                            handleChangeCode={(value) => setCode(value)}
                            codeVal={code}
                        />
                        <TextArea
                            label={t('formFiled:comment')} 
                            handleChange={(e) => setComments(e.target.value)} 
                            placeholder={t('formFiled:write message')}
                            />
                        <SubmitBtn
                            disabled={submitStatus}
                            content={t('formFiled:register')} />
                    </Box>
                </Box>


            </FormBody>
            <FormModal title={modalTitle} content={modalContent} btnContent={t('common:ok')} open={modalOpen} handleClose={handModalClose} />
        </FormContainer>
    )

}

export default withTranslation()(WrappedForm(ContactUs));