import React, { useState, useEffect } from 'react';
import FormContainer from '../../components/FormContainer'
import FormBody from '../../components/FormBody';
import TopicSelect from '../../components/TopicSelect';
import FormInput from '../../components/FormInput';
import FormPhoneInput from '../../components/FormPhoneInput';
import FormModal from '../../components/FormModal';
import SubmitBtn from '../../components/SubmitBtn';
import { withTranslation } from 'react-i18next';
import WrappedForm from '../../HOC/WrappedForm';

function Webinar(props) {
    const { t, requestHost } = props;
    const options = t('webinar:topics');
    const { firstName, lastName, email, phone, code, submitStatus, modalOpen, modalContent, modalTitle } = props;
    const { setFirstName, setLastName, setEmail, setCountryCode, setPhone, setCode, setSubmitStatus } = props;
    const { handModalClose, webinerRegister, selectRef, selectorId } = props;

    useEffect(() => {
        if (firstName.validate && lastName.validate && email.validate && phone.validate && code.validate) {
            setSubmitStatus(false)
        } else {
            setSubmitStatus(true)
        }
    }, [firstName, lastName, email, phone, code])


    return (
        <FormContainer>
            <FormBody handleSubmit={webinerRegister}>
                <TopicSelect
                    name='course'
                    label={t('webinar:topic')}
                    options={options}
                    selectRef={selectRef}
                    selectorId={selectorId}
                />
                <FormInput
                    required={true}
                    label={t('formFiled:firstName')}
                    handleChange={(value) => setFirstName(value)}
                    val={firstName}
                    type={'firstName'}
                />
                <FormInput
                    required={true}
                    label={t('formFiled:lastName')}
                    handleChange={(value) => setLastName(value)}
                    val={lastName}
                    type={'lastName'}
                />
                <FormInput
                    required={true}
                    label={t('formFiled:email')}
                    type='email'
                    handleChange={(value) => setEmail(value)}
                    val={email}
                />
                <FormPhoneInput
                    requestHost={requestHost}
                    handleCountryChange={(value) => setCountryCode(value)}

                    handleChangePhone={(value) => setPhone(value)}
                    phoneVal={phone}

                    handleChangeCode={(value) => setCode(value)}
                    codeVal={code}
                />
                <SubmitBtn
                    disabled={submitStatus}
                    content={t('formFiled:register')} />
            </FormBody>
            <FormModal title={modalTitle} content={modalContent} btnContent={t('common:ok')} open={modalOpen} handleClose={handModalClose} />
        </FormContainer>
    )

}

export default withTranslation()(WrappedForm(Webinar));