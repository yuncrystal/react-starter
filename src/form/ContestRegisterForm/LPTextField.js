
import React, { useState, useEffect, Fragment } from 'react';
import TextField from '@material-ui/core/TextField';
import validator from 'validator';
import Box from '@material-ui/core/Box';
import { parsePhoneNumberFromString } from 'libphonenumber-js'
import PropTypes from 'prop-types';

import { withStyles, makeStyles } from '@material-ui/core/styles';


const InputText = withStyles({
    root: {
        '& .MuiFormLabel-root': {
            color: '#fff',
            fontSize: 20,
            fontWeight: 500,
        },
        '& label.Mui-focused': {
            color: '#fff',
            opacity: '0.6',
            fontWeight: 500,
        },

        '& input.MuiInputBase-input': {
            fontSize: 20,
            fontWeight: '500',
            color: '#fff',
        },

        '& .MuiInput-underline': {
            borderBottom: '1px solid #7B7B7B'
        },
        '& .MuiInput-underline:after': {
            borderBottom: '1px solid #fff'
        },
    },
})(TextField);


const useStyles = makeStyles(theme => {
    return {
        row: {
            padding: '0 8px',
            margin: '8px 0'
        },
        root: {
            width: '100%',
        },
        error: {
            fontSize: 16,
            color: 'red',
            position: 'absolute'
        },
    }
})

const LPTextField = ({ label, name, val, handleChange, type,countryCode }) => {
    const classes = useStyles();
    return (
        <Box className={classes.row}>
            <InputText className={classes.root}
                label={label}
                name={name}
                onChange={(e) => handleChange({
                    ...val,
                    value: e.target.value
                })}
                onBlur={(e) => {
                    let value = e.target.value.trim();
                   
                    if (!value) {
                        handleChange({
                            ...val,
                            validate: false
                        })
                    } else {
                        if (type === 'phone') {
                            const phoneNumber = parsePhoneNumberFromString(value, countryCode);
                            if (phoneNumber) {
                                handleChange({
                                    ...val,
                                    validate: true
                                })
                            } else {
                                handleChange({
                                    ...val,
                                    validate: false,
                                    error:'手机号格式错误'
                                })
                            }
                        } else if (type === 'email') {
                            if (validator.isEmail(value)) {
                                handleChange({
                                    ...val,
                                    validate: true
                                })
                            } else {
                                handleChange({
                                    ...val,
                                    validate: false,
                                    error:'电邮格式错误'
                                })
                            }
                        } else if (type === 'url') {
                            if (validator.isURL(value)) {
                                handleChange({
                                    ...val,
                                    validate: true
                                })
                            } else {
                                handleChange({
                                    ...val,
                                    validate: false,
                                    error:'链接格式错误'
                                })
                            }
                        }
                        else {
                            handleChange({
                                ...val,
                                validate: true
                            })
                        }
                    }
                }}
            />
            <Box display={val.validate == null ? 'none' : val.validate ? 'none' : 'block'} className={classes.error}>{val.error}</Box>
        </Box>
    )
}

LPTextField.prototype = {
    label: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    val: PropTypes.object.isRequired,
    handleChange: PropTypes.func.isRequired,
    type: PropTypes.oneOf(['text', 'email', 'phone', 'url']),
    countryCode: PropTypes.string,
}

LPTextField.defaultProps = {
    type: 'text'
};

export default LPTextField;

