import React, { useState, useEffect, Fragment } from 'react';
import { MuiPickersUtilsProvider, DatePicker } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import moment from 'moment';
import PropTypes from 'prop-types';
import { createMuiTheme } from "@material-ui/core";
import { withStyles, makeStyles, ThemeProvider } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => {
    return {
        datePicker: {
            outlineWidth: 0,
            marginRight: '8px',
            marginLeft: '8px',
            '& .MuiInput-underline': {
                borderBottom: '1px solid #7B7B7B',
            },
            '& .MuiInputBase-input': {
                borderBottom: '1px solid #7B7B7B',
            },
            '& .MuiInputBase-input:after': {
                borderBottom: '1px solid #fff'
            },
            '& .MuiInput-underline:after': {
                borderBottom: '1px solid #fff'
            },

            '& label': {
                color: '#FFFFFF',
                fontSize: 20,
                fontWeight: '500',
                lineHeight: '29px',
                transform: 'scale(1)'
            },
            '& label.Mui-focused': {
                color: '#fff',
                opacity: '0.6',
                fontSize: 14,
                fontWeight: 500,
                lineHeight: '20px'
            },
            '& input': {
                marginTop: 20,
                color: "#fff",
            },

            '& button': {
                color: '#fff'
            }

        },
    }
})


const materialTheme = createMuiTheme({
    overrides: {
        MuiInput: {
            underline: {
                borderBottom: '1px solid #7B7B7B',
                '&:after': {
                    borderBottom: '1px solid #FFF'
                }
            }
        },

        MuiPickersToolbar: {
            toolbar: {
                backgroundColor: '#D8322A',
            },
        },
        MuiPickersCalendarHeader: {
            switchHeader: {
                // backgroundColor: lightBlue.A200,
                // color: "white",
            },
        },
        MuiPickersDay: {
            day: {
                color: '#D8322A',
            },
            daySelected: {
                backgroundColor: '#D8322A',
            },
            dayDisabled: {
                color: '#D8322A',
            },
            current: {
                color: '#D8322A',
            },
        },
        MuiPickersModal: {
            dialogAction: {
                color: '#D8322A',
            },
        },
    },
});

const BirthDayPicker = ({ val, handleBirthdayChange, setShowGuardian, setGuardianCountryCode }) => {
    const classes = useStyles();

    return (
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <ThemeProvider theme={materialTheme}>
                <DatePicker
                    className={classes.datePicker}
                    variant="inline"
                    format="MM/dd/yyyy"
                    label="出生日期"
                    helperText="请选择出生日期"
                    value={val}
                    onChange={(e) => {
                        let age = moment().diff(moment(e, "MM-DD-YYYY").format(), 'years');
                        handleBirthdayChange(e)
                        if (age < 18) {
                            setShowGuardian(true);
                            setGuardianCountryCode('AU');
                        } else {
                            setShowGuardian(false);
                            setGuardianCountryCode('');
                        }
                    }}
                />
            </ThemeProvider>
        </MuiPickersUtilsProvider>
    )
}

BirthDayPicker.prototype = {

    val: PropTypes.object.isRequired,
    handleBirthdayChange: PropTypes.func.isRequired,
    setShowGuardian: PropTypes.func.isRequired,
    setGuardianCountryCode: PropTypes.func.isRequired,
}


export default BirthDayPicker;