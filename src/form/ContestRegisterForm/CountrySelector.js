import React, { useState, useEffect, Fragment } from 'react';
import PropTypes from 'prop-types';
import Select from '@material-ui/core/Select';
import { makeStyles } from '@material-ui/core/styles';
import i18nIsoCountries from 'i18n-iso-countries';
import MenuItem from '@material-ui/core/MenuItem';
import libphonenumber from '../../libphonenumber.full.json';

const useStyles = makeStyles((theme) => {
    return {
        selector: {
            borderBottom: '1px solid #7B7B7B',
            margin: '30px 8px 0',
            outlineWidth: 0,
            color: '#FFFFFF',
            fontSize: 20,
            fontWeight: '500',
            lineHeight: '29px',
            '&:after': {
                borderBottom: '1px solid #fff'
            },
            "& svg": {
                backgroundColor: "#000",
                color: '#9B9B9B'
            }
        },
        countryItem: {
            outlineWidth: 0,
        },
    }
})


const CountrySelector = ({val,handleCountryCodeChange,handleCountryChange }) => {
    const classes = useStyles();
    const { countries } = libphonenumber;
    return (
        <Select
            className={classes.selector}
            value={val}
            onChange={(e) => {
                handleCountryCodeChange(e.target.value)
                handleCountryChange(e.target.value);
            }}>

            {
                _.map(countries, (value,key) => {
                    return <MenuItem className={classes.countryItem} key={key} value={key}>{i18nIsoCountries.getName(key, 'zh')}</MenuItem>
                })
            }
        </Select>
    )
}

CountrySelector.prototype = {
    val: PropTypes.object.isRequired,
    handleCountryCodeChange: PropTypes.func.isRequired,
    handleCountryChange: PropTypes.func.isRequired,
    
}

export default CountrySelector;