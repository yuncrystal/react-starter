import 'date-fns';
import React, { useState, useEffect, Fragment } from 'react';
import FormContainer from '../../components/FormContainer'
import FormBody from '../../components/FormBody';
import Box from '@material-ui/core/Box';
import { API_URL } from '../../api/apiUrl';
import FormAPI from '../../api/form_api';
import LPTextField from './LPTextField';
import BirthDayPicker from './BirthDayPicker';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import libphonenumber from '../../libphonenumber.full.json';
import i18nIsoCountries from 'i18n-iso-countries';
import moment from 'moment';
import PopUp from './popUp';
import CountrySelector from './CountrySelector';
import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles(theme => {
    return {
        root: {
            display: 'flex',
            flexWrap: 'wrap',
        },
        label: {
            fontSize: 20,
            fontWeight: '500',
            lineHeight: '29px',
            color: '#fff',
            marginTop: 32,
            marginLeft: theme.spacing(1),
        },
        select: {
            width: '23.5%',
            borderBottom: '1px solid #7B7B7B',
            margin: '30px 8px 8px',
            outlineWidth: 0,
            color: '#FFFFFF',
            fontSize: 20,
            fontWeight: '500',
            lineHeight: '29px',
            [theme.breakpoints.down('768')]: {
                width: 'calc(100% - 16px)',
                margin: '30px auto 0',
            },
            '&:after': {
                borderBottom: '1px solid #fff'
            },
            "& svg": {
                backgroundColor: "#000",
                color: '#9B9B9B'
            }
        },

        phoneWrap: {
            width: '70.5%',
            position: 'relative',
            [theme.breakpoints.down('768')]: {
                width: '100%',
            }
        },

        phone: {
            width: '100%'
        },

        code: {
            margin: theme.spacing(1),
            width: 'calc(100% - 16px)'
        },

        countryContainer: {
            display: 'flex',
            alignItems: 'flex-end',
            justifyContent: 'space-between',
            [theme.breakpoints.down('768')]: {
                flexDirection: 'column',
                paddingRight: 0
            }
        },
        sendCode: {
            fontSize: 20,
            fontWeight: '500',
            color: "rgba(255,255,255,0.6)",
            position: 'absolute',
            right: 8,
            bottom: 15,
            cursor: 'pointer'

        },

        submitWrap: {
            display: 'flex',
            justifyContent: 'flex-end'
        },
        submit: {
            height: 51,
            width: 144,
            borderRadius: 24,
            background: '#D8322A',
            border: 0,
            fontSize: 20,
            fontWeight: '500',
            lineHeight: '29px',
            color: "#fff",
            outlineWidth: 0,
            marginTop: 48,
            cursor: 'pointer',
            '&:hover': {
                background: '#B72A23',
            },
            [theme.breakpoints.down('768')]: {
                width: 'calc(100% - 16px)',
                margin: '48px auto 0',
            }
        }
    }
});




function ContestRegisterForm(props) {
    const classes = useStyles();
    const { countries } = libphonenumber;

    const [firstName, setFirstName] = useState({ value: '', validate: null, error: '请输入名' });
    const [lastName, setLastName] = useState({ value: '', validate: null, error: '请输入姓' });
    const [country, setCountry] = useState('');
    const [countryCode, setCountryCode] = useState('');
    const [phone, setPhone] = useState({ value: '', validate: null, error: '请输入手机' });
    const [code, setCode] = useState({ value: '', validate: null, error: '请输入验证码' });
    const [email, setEmail] = useState({ value: '', validate: null, error: '请输入电邮' });
    const [videoUrl, setVideoUrl] = useState('');
    const [birthday, setBirthday] = useState('');
    const [codeHidden, setCodeHidden] = useState(false);


    const [showGuardian, setShowGuardian] = useState(false);
    const [modalOpen, setModalOpen] = useState(false);
    const [modalType, setModalType] = useState(true);


    // 监护人信息
    const [guardianFirstName, setGuardianFirstName] = useState({ value: '', validate: null, error: '请输入监护人名' });
    const [guardianLastName, setGuardianLastName] = useState({ value: '', validate: null, error: '请输入监护人姓' });

    const [guardianCountryCode, setGuardianCountryCode] = useState('');
    const [guardianPhone, setGuardianPhone] = useState({ value: '', validate: null, error: '请输入监护人电话' });


    useEffect(() => {
        setCountry('AU');
        setCountryCode('AU');

    }, [])

    useEffect(() => {
        if (countryCode === 'VN' || countryCode === 'UK' || countryCode === 'HU') {
            setCodeHidden(true)
        }else {
            setCodeHidden(false)
        }


    }, [countryCode])

    const handleModalOpen = () => {
        setModalOpen(true);
    };

    const handModalClose = () => {
        setModalOpen(false);
    }


    const handleRegister = async () => {
        event.preventDefault()

        let guardianContactNum = '';
        if (guardianCountryCode !== '') {
            guardianContactNum = countries[guardianCountryCode][0].toString() + guardianPhone.value
        }
        const data = {
            first_name: firstName.value,
            last_name: lastName.value,
            email: email.value,
            country_code: countries[countryCode][0],
            phone: phone.value,
            country: i18nIsoCountries.getName(country, 'en'),
            code: code.value,
            video_url: videoUrl.value,
            birthDay_year: moment(birthday).get('year'),
            birthDay_month: moment(birthday).get('month'),
            birthDay_day: moment(birthday).get('date'),
            landing_page_url: window.location.href,
            guardian_first_name: guardianFirstName.value,
            guardian_last_name: guardianLastName.value,
            guardian_contact_number: guardianContactNum
        }
        try {
            const response = await FormAPI.post(API_URL.LANDING_PAGE.JAY_CHOU_CONTEST, data)
            setModalType(true)
            handleModalOpen();

        } catch (err) {
            setModalType(false)
            handleModalOpen();
        }

    }

    const getValidationCode = async (e) => {
        e.preventDefault();
        const response = await FormAPI.post(API_URL.PHONE.VALIDATION, {
            phone: phone.value,
            country: countries[countryCode][0]
        })
        if (response.data.status_code >= 400) {
            console.log('response.data:', response.data);
        } else {
        }
    }

    return (
        <FormContainer>
            <FormBody handleSubmit={handleRegister} background='#000'>
                <LPTextField
                    label={'名'}
                    name={'firstName'}
                    val={firstName}
                    handleChange={setFirstName} />

                <LPTextField
                    label={'姓'}
                    name={'lastname'}
                    val={lastName}
                    handleChange={setLastName} />

                <CountrySelector
                    val={country}
                    handleCountryCodeChange={setCountryCode}
                    handleCountryChange={setCountry} />


                <div className={classes.label}>手机</div>
                <Box className={classes.countryContainer}>

                    <Select
                        className={classes.select}
                        value={countryCode}
                        onChange={(e) => {
                            setCountryCode(e.target.value)
                        }}>
                        {
                            _.map(countries, (value, key) => {
                                return <MenuItem className={classes.countryItem} key={key} value={key}>{i18nIsoCountries.getName(key, 'zh')} +{value[0]}</MenuItem>
                            })
                        }

                    </Select>
                    <Box className={classes.phoneWrap}>
                        <LPTextField
                            label={'手机号'}
                            name={'phone'}
                            val={phone}
                            type={'phone'}
                            countryCode={countryCode}
                            handleChange={setPhone} />
                    </Box>

                </Box>

                <Box display={codeHidden ? 'none' : 'block'} width='100%' position='relative'>
                    <LPTextField
                        label={'验证码'}
                        name={'code'}
                        val={code}
                        handleChange={setCode} />
                    <div className={classes.sendCode} onClick={getValidationCode}>发送验证码</div>
                </Box>

                <LPTextField
                    label={'电邮'}
                    name={'email'}
                    val={email}
                    handleChange={setEmail} />

                <LPTextField
                    label={'参赛作品链接'}
                    name={'video_url'}
                    val={videoUrl}
                    handleChange={setVideoUrl} />

                <BirthDayPicker
                    val={birthday}
                    handleBirthdayChange={setBirthday}
                    setShowGuardian={setShowGuardian}
                    setGuardianCountryCode={setGuardianCountryCode} />


                <Box display={showGuardian ? 'flex' : 'none'} flexDirection='column'>
                    <Box className={classes.label}>由于您的年龄在18岁之下，因此需要填写您的监护人信息。</Box>
                    <Box className={classes.label}>监护人信息</Box>
                    <LPTextField
                        label={'名'}
                        name={'guardianFirstName'}
                        val={guardianFirstName}
                        handleChange={setGuardianFirstName} />
                    <LPTextField
                        label={'姓'}
                        name={'guardianLastName'}
                        val={guardianLastName}
                        handleChange={setGuardianLastName} />

                    <div className={classes.label}>手机</div>
                    <Box className={classes.countryContainer}>
                        <Select
                            className={classes.select}
                            value={guardianCountryCode}
                            onChange={(e) => {
                                setGuardianCountryCode(e.target.value)
                            }}>
                            {
                                _.map(countries, (value, key) => {
                                    return <MenuItem className={classes.countryItem} key={key} value={key}>{i18nIsoCountries.getName(key, 'zh')} +{value[0]}</MenuItem>
                                })
                            }

                        </Select>
                        <Box className={classes.phoneWrap}>
                            <LPTextField
                                label={'手机号'}
                                name={'guardianPhone'}
                                val={guardianPhone}
                                type={'phone'}
                                countryCode={guardianCountryCode}
                                handleChange={setGuardianPhone} />
                        </Box>
                    </Box>
                </Box>

                <Box className={classes.submitWrap}>
                    <input className={classes.submit} type="submit" value={'立即报名'} />
                </Box>
            </FormBody>
            <PopUp open={modalOpen} type={modalType} handleClose={handModalClose} />
        </FormContainer >
    )

}

export default ContestRegisterForm;