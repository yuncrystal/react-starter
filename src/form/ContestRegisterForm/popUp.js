
import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Close from './close.png';
import Modal from '@material-ui/core/Modal';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles((theme) => {
    return {
        paper: {
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            margin: 'auto',
            width: 640,
            maxHeight: 400,
            minHeight: 364,
            borderRadius: 8,
            backgroundColor: '#252525',
            padding: '20px 32px 56px',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'space-between',
            
            '&:focus': {
                outlineWidth: 0
            },
            [theme.breakpoints.down('768')]: {
                width: 'calc(100% - 32px)',
                padding: '40px 10px',
                maxHeight:420
            }
        },
        title: {
            fontSize: 28,
            color: '#FFFFFF',
            fontWeight: 500,
            lineHeight: '41px',
            marginBottom: 16
        },
        content: {
            fontSize: 24,
            lineHeight: '36px',
            color: 'rgba(255,255,255,0.8)',
            textAlign: 'center',
            marginBottom: 56,
            width: '70%',
            [theme.breakpoints.down('768')]: {
                width: '100%',
            }
        },
        closeIcon:{
            marginBottom:16,
            display:'flex',
            width:'100%',
            justifyContent:'flex-end',
            cursor: 'pointer',
            '& img':{
                width:25,
                height:25
            },
            '& i':{
                fontSize:35,
                color:'#7c7c7c'
            }
        },
        closeBtn: {
            height: 51,
            width: 104,
            fontSize: 20,
            borderRadius: 24,
            color: '#FFFFFF',
            backgroundColor: props=>props.type ? '#449C20' :'#CD5614',
            cursor: 'pointer',
            fontWeight: 500,
            padding: 0,
            border: 0,
            outlineWidth: 0,
            '&:hover': {
                backgroundColor: props=>props.type ? '#367C19' :'#A44410',
            }
            
        }
    }
})

const PopUp = ({ open, handleClose, type }) => {
    const classes = useStyles({type:type});
    const errorTitle = '系统错误';
    const errorMessage = '后台或验证码错误,请重试或联系我们的客户销售团队。';
    const successTitle = '报名成功';
    const successMessage = ' 感谢您的参与，我们十分珍视您的才艺表演。祝您在本次比赛中获得来到悉尼现场近距离接触周杰伦的机会！';

    return (
        <Modal
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            open={open}
            onClose={handleClose}
        >
            <Typography className={classes.paper} component="div">
                <div className={classes.closeIcon} onClick={handleClose}>
                    <i className="fas fa-times"></i>
                </div>
                <div className={classes.title}>{type ? successTitle : errorTitle}</div>
                <div className={classes.content}>{type ? successMessage : errorMessage}</div>
                <button className={classes.closeBtn} onClick={handleClose}>确认</button>
            </Typography>
        </Modal>
    )

}


PopUp.propTypes = {
    type: PropTypes.bool.isRequired,
    open: PropTypes.bool.isRequired,
    handleClose: PropTypes.func.isRequired,
};
export default PopUp;