import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
import Webinar from './form/Webinar';
import ContactUs from './form/ContactUs';
import ContestRegister from './form/ContestRegisterForm';
import _ from 'lodash';
import i18n from './i18n';
import './main.scss';
import i18nIsoCountries from 'i18n-iso-countries';

i18nIsoCountries.registerLocale(require("i18n-iso-countries/langs/en.json"));
i18nIsoCountries.registerLocale(require("i18n-iso-countries/langs/zh.json"));
i18nIsoCountries.registerLocale(require("i18n-iso-countries/langs/id.json"));
i18nIsoCountries.registerLocale(require("i18n-iso-countries/langs/vi.json"));

export function FormContactUs({ documentID, lang }) {
    i18n.changeLanguage(lang)
    ReactDOM.render(
        <Suspense fallback=''>
            <ContactUs />
        </Suspense>

        , documentID);
}

export function ContestRegisterForm({ documentID, lang }) {
    i18n.changeLanguage(lang)
    ReactDOM.render(
        <Suspense fallback=''>
            <ContestRegister />
        </Suspense>

        , documentID);
}

export function FormWebinar({ documentID, lang, selectorId }) {

    i18n.changeLanguage(lang)
    ReactDOM.render(
        <Suspense fallback=''>
            <Webinar selectorId={selectorId} />
        </Suspense>
        , documentID);
}

// export class Form {
//     constructor({
//         documentID, lang, selectorId
//     }){
//         i18n.changeLanguage(lang)
//         ReactDOM.render(
//             <Suspense fallback=''>
//                 <Webinar selectorId={selectorId} />
//             </Suspense>
//             , documentID);
//     }

//     sayHello(){
//         console.log('sayHello');
//     }
// }