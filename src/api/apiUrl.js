export const API_URL = {
    PHONE:{
        VALIDATION:'/get-phone-validation'
    },
    WEBINAR:{
        REGISTER:'/webinar'
    },
    CONTACTUS:{
        CONTACT:'/contact-form'
    },
    LANDING_PAGE:{
        JAY_CHOU_CONTEST:'/landing-page/jay-chou-contest'
    }
}

