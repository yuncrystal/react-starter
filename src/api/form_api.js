import axios from 'axios';

let baseURL

baseURL = process.env.host + 'v1';

console.log('baseURL:', process.env.host)
const form_api = axios.create({
    baseURL
})

export default form_api;

