const ID = {
    "common": {
        "close": "Close",
        "ok":"OK",
        "next":"Next step"
    },
    "formFiled": {
        "firstName": "First Name",
        "lastName": "Last Name",
        "email": "Email",
        "phone": "Phone",
        "authCode": "Mobile Authentication",
        "register": "Register",
        "getCode": "Get Code",
        "sent": "Sent",
        "comment":"Type a message",
        "optional":"(optional)",
        "write message":"Write message here...",
        "fieldRequired": "This field is required*",
        "first_name_error": "Invalid first name.(max length 40)",
        "last_name_error": "Invalid last name.(max length 80)",
        "error_email": "The email format is wrong",
        "error_phone": "The phone format is wrong",
        "error_phone_exist": "The phone number is not exist",
        "error_code": "The authentication code is wrong",
        "sent_code_success": "The verification code has been sent to your phone",
        "modalTitle": "Terimakasih atas pertanyaan anda.",
        "modalContent": "Aplikasi anda telah diterima, salah satu manajer akun kami akan menghubungi anda dalam 24 jam! Kami berharap pengalaman trading yang berkesan untuk anda.",
        "modalTitleFail":"Error",
        "modalContentFail":"Please contact our customer service team",
        "personalDetail":"Personal Details",
        "Confirmation":"Confirmation"
    },
    "webinar": {
        "title": "Register for a Webinar now",
        "subTitle": "Please fill in the form below and you will be contacted by one of our professional business experts.",
        "topic": "Topic",
        "topics": [
            '04/10/19 - NFP - Live Trading Webinar',
            '08/10/19 - Brexit & the Euro in the spotlight',
            '10/10/19 - USD special to profit from FX Fundamentals',
            '15/10/19 - A strategy playbook for day trading FX & Gold',
            '17/10/19 - Aussie Dollar update & FX opportunities',
            '22/10/19 - Review of FX majors & Trading opportunities this week',
            '24/10/19 - How to play Euro around the ECB',
            '29/10/19 - USD & Fed preview for FX & Gold traders',
            '31/10/19 - A structured approach to deciphering FX & Gold sentiment',
        ],
        
    },
    "contactUs": {
        "title": "Contact Form",
        "subTitle": "If you have a question or need help, please fill in your details below, and we will get back to you right away."
    }


}

export default ID