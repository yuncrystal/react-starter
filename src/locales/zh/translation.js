const ZH = {
    "common": {
        "close": "关闭",
        "ok": "确认",
        "next":"下一步"
    },
    "formFiled": {
        "firstName": "名",
        "lastName": "姓",
        "email": "电邮",
        "phone": "手机号码",
        "authCode": "验证码",
        "register": "注册",
        "getCode": "验证码",
        "getCodeAbbr": "验证码",
        "sent": "已发送",
        "comment":"请在这里留言",
        "optional":"(可选)",
        "write message":"请在这里留言。。。",
        "fieldRequired": "这是必填字段*",
        "first_name_error": "非法名（最长40字符）",
        "last_name_error": "非法姓（最长80字符）",
        "error_email": "邮箱格式错误",
        "error_phone": "手机格式错误",
        "error_phone_exist": "手机号不存在",
        "error_code": "验证码错误",
        "sent_code_success": "验证码已发送到你的手机",
        "modalTitle": "ACY稀万证券感谢您的询问",
        "modalContent": "您的申请已收到，我们会在24小时内安排客户经理与您联系。我们希望您有一个愉快的交易体验",
        "modalTitleFail":"Error",
        "modalContentFail":"请与我们联系",
        "personalDetail":"个人信息",
        "Confirmation":"确认"
    },
    "webinar": {
        "title": "注册网络讲座",
        "subTitle": "如果您需要了解更多关于网络讲座，请在下面填写您的详细信息及所需课程， 我们将尽快与您联系。",
        "topic": "课程",
        "topics": [
            '01/11/19 - 大战“非农之夜”',
            '04/11/19 - 外汇市场月度展望',
            '07/11/19 - 《交易员的自我修养》',
            '11/11/19 - 外汇市场当周展望',
            '14/11/19 - 《交易员的自我修养》',
            '18/11/19 - 外汇市场当周展望',
            '21/11/19 - 《交易员的自我修养》',
            '25/11/19 - 外汇市场当周展望',
            '28/11/19 - 《交易员的自我修养》'
        ],
    },
    "contactUs": {
        "title": "Contact Form",
        "subTitle": "If you have a question or need help, please fill in your details below, and we will get back to you right away.",
    }
}
export default ZH;