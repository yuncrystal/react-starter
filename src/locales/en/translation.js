const EN = {
    "common": {
        "close": "Close",
        "ok": "OK",
        "next":"Next step"
    },
    "formFiled": {
        "firstName": "First Name",
        "lastName": "Last Name",
        "email": "Email",
        "phone": "Phone",
        "authCode": "Mobile Authentication",
        "register": "Register",
        "getCode": "Get Code",
        "getCodeAbbr": "Get",
        "sent": "Sent",
        "comment":"Type a message",
        "optional":"(optional)",
        "write message":"Write message here...",
        "fieldRequired": "This field is required*",
        "first_name_error": "Invalid first name.(max length 40)",
        "last_name_error": "Invalid last name.(max length 80)",
        "error_email": "The email format is wrong",
        "error_phone": "The phone format is wrong",
        "error_phone_exist": "The phone number is not exist",
        "error_code": "The authentication code is wrong",
        "sent_code_success": "The verification code has been sent to your phone",
        "modalTitle": "Thank You For Inquiring ACY.",
        "modalContent": "Your application has been received, and we will arrange an account manager within 24 hours to contact you! We wish you have pleasant trading experience!",
        "modalTitleFail":"Error",
        "modalContentFail":"Please contact our customer service team",
        "personalDetail":"Personal Details",
        "Confirmation":"Confirmation"
    },
    "webinar": {
        "title": "Register for a Webinar now",
        "subTitle": "Please fill in the form below and you will be contacted by one of our professional business experts.",
        "topic": "Topic",
        "topics": [
            '12/11/2019 - How to dig out high probability FX trades.',
            '14/11/2019 - A live market scan demonstrating FX market correlations.',
            '19/11/2019 - How to find trading opportunities in FX majors.',
            '21/11/2019 - A technical & fundamental scan of FX & Gold.',
            '26/1/2019 - A deep dive into how you can accurately decipher the FX & Gold sentiment.',
            '28/11/2019 - A Live trading session for USD & Gold demonstrating high probability trades.',
        ]
    },
    "contactUs": {
        "title": "Contact Form",
        "subTitle": "If you have a question or need help, please fill in your details below, and we will get back to you right away."
    },
}
export default EN