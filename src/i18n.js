import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import EN from './locales/en/translation.js';
import ZH from './locales/zh/translation.js';
import VI from './locales/vi/translation.js';
import ID from './locales/id/translation.js';


// the translations
// (tip move them in a JSON file and import them)
const resources = {
  en: EN,
  zh: ZH,
  id: ID,
  vi: VI
};


i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources,
    lng: "en",
    debug: true,
    returnObjects: true,
    interpolation: {
      escapeValue: false // react already safes from xss
    }
  });

export default i18n;